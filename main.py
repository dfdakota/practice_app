from flask import Flask, render_template

app = Flask(__name__, template_folder='templates', static_folder='static')


class GalileanMoons:
    def __init__(self, first, second, third, fourth):
        self.first = first
        self.second = second
        self.third = third
        self.fourth = fourth


@app.route('/')
def base_page():
    return render_template(
        'jinja_intro.html', name2="Bob Smith", template_name2="Jinja2")


@app.route("/for-loop/conditionals/")
def render_for_loop_conditionals():
    user_os = {
        "Bob": "MX Linux",
        "Anne": "Kubuntu",
        "Joe": "Debian",
        "Jose": "Kali",
    }
    return render_template("loops_and_conditionals.html", user_os=user_os)


@app.route("/for-loop/")
def render_loops_for():
    planets = [
        "Mercury",
        "Venus",
        "Earth",
        "Mars",
        "Jupiter",
        "Saturn",
        "Uranus",
        "Neptune",
    ]
    return render_template("for_loop.html", planets=planets)


@app.route('/conditional-basics/')
def render_conditionals():
    company = "Microsoft"
    return render_template("conditionals_basics.html", company=company)


@app.route("/data-structures/")
def render_data_structures():
    movies = [
        "Home Alone",
        "Up",
        "It's a Wonderful Life",
    ]

    car = {
        "brand": "Ford",
        "model": "F150",
        "year": "2022",
    }

    moons = GalileanMoons("Io", "Europa", "Ganymede", "Callisto")

    kwargs = {
        "movies": movies,
        "car": car,
        "moons": moons,
    }

    return render_template("data_structures.html", **kwargs)


@app.route("/expressions/")
def sample_expressions():
    #  interpolation
    color = "brown"
    animal_one = "fox"
    animal_two = "dog"

    #  addition and subtraction
    orange_amount = 10
    apple_amount = 20
    donate_amount = 15

    # string concatenation
    first_name = "Captain"
    last_name = "Marvel"

    kwargs = {
        "color": color,
        "animal_one": animal_one,
        "animal_two": animal_two,
        "orange_amount": orange_amount,
        "apple_amount": apple_amount,
        "donate_amount": donate_amount,
        "first_name": first_name,
        "last_name": last_name,
    }
    return render_template('jinja_expressions.html', **kwargs)


@app.route('/2/')
def page_2():
    return render_template('page_2.html')


@app.route('/3/')
def page_3():
    return render_template('page_3.html')


if __name__ == "__main__":
    app.run()
